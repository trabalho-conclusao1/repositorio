from django.db import models
from datetime import datetime
from django.contrib.auth.models import User


class Newsletter(models.Model):

    email = models.EmailField()
    active_receive = models.BooleanField()
    date_created = models.DateTimeField(default=datetime.now, blank=True)


    class Meta:
        db_table = 'newsletter'
        ordering = ['-date_created']


    def __str__(self):
        return f'{self.email}'


class Contact(models.Model):

    LOAN_STATUS = (
        ('n', 'Aguardando'),
        ('p', 'Pendente'),
        ('s', 'Finalizado'),
    )

    name = models.CharField(max_length=60)
    email = models.EmailField(unique=True)
    message = models.TextField(max_length=250)
    type_contact = models.CharField(max_length=1)
    date_contact = models.DateTimeField(default=datetime.now, blank=True)
    status = models.CharField(max_length=1, choices=LOAN_STATUS, default='n')


    class Meta:
        db_table = 'contact'
        ordering = ['-date_contact']


    def __str__(self):
        return f'{self.name}'


class Course(models.Model):

    description = models.CharField(max_length=60)
    active_course = models.BooleanField(default=True)


    class Meta:
        db_table = 'course'
        ordering = ['description']


    def __str__(self):
        return f'{self.description}'


class Publication(models.Model):

    LOAN_STATUS = (
        ('a', 'Artigo'),
        ('m', 'Monografia'),
        ('r', 'Relatório Técnico'),
        ('t', 'Tese'),
    )

    summary = models.CharField(max_length=60)
    abstract = models.TextField(max_length=1500)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    contributor = models.CharField(max_length=60, blank=True, null=True)
    genre = models.CharField(max_length=1, choices=LOAN_STATUS)
    date_created = models.DateTimeField(default=datetime.now, blank=True)
    date_published = models.DateTimeField(default=datetime.now, blank=True)
    open_access = models.BooleanField()
    educational_institution = models.CharField(max_length=100, blank=True, null=True)
    file_publication = models.FileField(upload_to='documents/%Y/%m/')    
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    item_published = models.BooleanField(default=False)


    class Meta:
        db_table = 'publication'
        ordering = ['-date_published']


    def __str__(self):
        return f'{self.summary}, {self.author}'