from django.urls import path

from . import views


urlpatterns = [
    path('views/article/<int:id_publication>', views.article, name='article'),
    path('submission/', views.submission, name='submission'),
    path('views/submission/', views.my_submissions, name='my_submissions'),
    path('submission/new/', views.create_submission, name='create_submission'),
    path('views/<int:id_course>/publication/', views.publication, name='publication'),
    path('views/items-collections/', views.collection, name='collection'),
]
