from django.contrib import admin
from core.models import Newsletter, Contact, Course, Publication


# Register your models here.

class NewsletterAdmin(admin.ModelAdmin):
    
    list_display = (
        'email',
        'active_receive',
        'date_created',
    )

    list_filter = (
        'email',
        'date_created',
    )

    search_fields = (
        'email',
    )

    list_display_links = (
        'email',
    )

    list_editable = (
        'active_receive',
    )

    list_per_page = 10


class ContactAdmin(admin.ModelAdmin):

    list_display = (
        'name',
        'email',
        'type_contact',
        'date_contact',
        'status',
    )

    list_filter = (
        'name',
        'email',
        'date_contact',
        'status',
    )

    list_editable = (
        'status',
    )

    list_display_links = (
        'name',
    )

    search_fields = (
        'email',
    )

    list_per_page = 10


class CourseAdmin(admin.ModelAdmin):

    list_display = (
        'description',
        'active_course',
    )

    list_filter = (
        'description',
        'active_course',
    )

    list_editable = (
        'active_course',
    )

    search_fields = (
        'description',
    )

    list_per_page = 10


class PublicationAdmin(admin.ModelAdmin):

    list_display = (
        'summary',
        'author',
        'contributor',
        'genre',
        'date_published',
        'course',
        'item_published',
    )

    list_filter = (
        'author',
        'date_published',
        'genre',
        'course',
        'item_published',
    )

    list_editable = (
        'genre',
        'course',
        'item_published'
    )

    list_display_links = (
        'summary',
        'author',
    )

    search_fields = (
        'summary',
        'author',
    )

    list_per_page = 10


admin.site.register(Newsletter, NewsletterAdmin)
admin.site.register(Contact, ContactAdmin)
admin.site.register(Course, CourseAdmin)
admin.site.register(Publication, PublicationAdmin)