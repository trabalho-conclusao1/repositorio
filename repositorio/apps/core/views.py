from django.shortcuts import render, redirect, get_object_or_404
from .models import Course, Publication
from django.contrib.auth.models import User
from django.contrib import messages
from repositorio.configs_rtf import navbar
from  django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def publication(request, id_course):
    """Busca a publicação de acordo com a opção do usuário."""

    course = get_object_or_404(Course, pk=id_course)
    publications = Publication.objects.filter(course=course).filter(item_published=True)

    paginator = Paginator(publications, 4)
    page = request.GET.get('page')
    publications_to_page = paginator.get_page(page)

    context = {
        'publications': publications_to_page,
        'course': course,
    }

    if 'search' in request.GET:
        search = request.GET['search']
        publications = publications.filter(summary__icontains=search)

        context['publications'] = publications

    return render(request, 'publications/publications.html', context)


def article(request, id_publication):
    """Exibe o artigo selecionado pelo usuário."""

    publication = get_object_or_404(Publication, pk=id_publication)

    nav = navbar()
    nav['article'] = 'active'
    context = {
        'context': nav,
        'publication': publication,
    }

    return render(request, 'publications/article.html', context)


def submission(request):
    """Realiza a publicação de forma automatica."""

    if request.user.is_authenticated:
        nav = navbar()
        nav['submissions'] = 'active'

        return render(request, 'publications/submissions.html', {'context': nav})

    else:
        return redirect('index')


def my_submissions(request):
    """Mostra apenas as submissões do usuário logado."""

    if request.user.is_authenticated:

        publications = Publication.objects.order_by('summary', '-date_published').filter(author=request.user.id)

        paginator = Paginator(publications, 2)
        page = request.GET.get('page')
        publications_to_page = paginator.get_page(page)

        nav = navbar()
        nav['my_submissions'] = 'active'
        context = {
            'context': nav,
            'publications': publications_to_page,
        }

        return render(request, 'publications/my_submissions.html', context)

    else:
        return redirect('index')


def create_submission(request):
    """Cria novas publicações no repositório."""

    if request.user.is_authenticated:
        course = Course.objects.filter(active_course=True)

        nav = navbar()
        nav['new_submission'] = 'active'

        context = {
            'context': nav,
            'courses': course,
        }

        if request.method == 'POST':

            summary     = request.POST['summary']
            abstract    = request.POST['abstract']
            contributor = request.POST['contributor']
            genre       = request.POST['genre']
            institute   = request.POST['institute']

            if request.POST.get('access_open', '') == 'on':
                access_open = True
            else:
                access_open = False

            docfile     = request.FILES['docfile']

            user = get_object_or_404(User, pk=request.user.id)
            course = get_object_or_404(Course, pk=request.POST['course'])

            Publication.objects.create(
                summary=summary.capitalize(),
                abstract=abstract,
                author=user,
                contributor=contributor,
                genre=genre,
                open_access=access_open,
                educational_institution=institute,
                file_publication=docfile,
                course=course,
            ).save()

            messages.success(
                request,
                'Parabéns! Publicação foi cadastrada com sucesso, agora falta pouco.'
            )
            return render(request, 'publications/new_submission.html', context)
        else:

            return render(request, 'publications/new_submission.html', context)

    else:
        return redirect('index')


def collection(request):
    """Exibe as coleções existentes no repositório da instituição"""

    courses = Course.objects.all()
    return render(request, 'publications/collection.html', {"cursos": courses})
