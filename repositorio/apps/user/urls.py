from django.urls import path

from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('signin/user/', views.signin, name='signin'),
    path('register/new/', views.register, name='register'),
    path('user/logout/', views.logout, name='logout'),
]