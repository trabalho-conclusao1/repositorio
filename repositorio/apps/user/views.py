from django.shortcuts import render, redirect #get_object_or_404
from django.contrib.auth.models import User
from django.contrib import messages, auth
from repositorio.configs_rtf import navbar
from core.models import Course, Publication, Newsletter


def signin(request):
    """Realiza o acesso e autenticação na aplicação."""

    if request.method == 'POST':

        email_user = request.POST['email']
        password_user = request.POST['password']

        if User.objects.filter(email=email_user).exists():
            username = User.objects.filter(email=email_user).values_list('username', flat=True).get()
            user = auth.authenticate(
                request,
                username=username,
                password=password_user
            )
        else:
            messages.error(
                request,
                'Email invalido! Informe o e-mail corretamente.'
            )
            return redirect('signin')

        if user is not None:
            auth.login(request, user)
            return redirect('my_submissions')
        else:
            messages.error(
                request,
                'E-mail/Senha invalidas! Tente novamente realizar o acesso!'
            )
            return redirect('signin')

    else:
        return render(request, 'users/signin.html')


def register(request):
    """Realiza um novo cadastro na aplicação."""

    if request.method == 'POST':

        first_name = request.POST['first-name']
        last_name  = request.POST['last-name']
        username   = request.POST['username']
        email      = request.POST['email']

        if request.POST['password'] == request.POST['confirm-password']:

            password = request.POST['password']
        else:

            messages.error(
                request,
                'Senhas erradas. As senhas precisam ser iguais!'
            )
            return redirect('register')

        if User.objects.filter(email=email).exists():

            messages.error(
                request,
                'E-mail invalido. O e-mail já possui cadastro em nossa base de dados!'
            )
            return redirect('register')
        else:
            if User.objects.filter(username=username).exists():

                messages.error(
                    request,
                    'Username invalido. O Username informado já possuir cadastro, escolha outro!'
                )
                return redirect('register')
            else:

                User.objects.create_user(
                    username=username,
                    first_name=first_name,
                    last_name=last_name,
                    email=email,
                    password=password,
                ).save()

                messages.success(
                    request,
                    'Parabéns! Cadastro realizado com sucesso.'
                )
                return redirect('signin')
    else:
        return render(request, 'users/register.html')


def logout(request):
    """Realiza a saída da aplicação."""

    auth.logout(request)
    return redirect('index')


def index(request):
    """Pagina inicial da aplicação."""

    if request.method == 'POST' and request.POST['email-newsletter']:
        email_newsletter = request.POST['email-newsletter']

        if not Newsletter.objects.filter(email=email_newsletter).exists():

            Newsletter.objects.create(
                email = email_newsletter,
                active_receive = True
            ).save()


    courses = Course.objects.all()
    publication = Publication.objects.filter(item_published=True)[:5]

    nav = navbar()
    nav['index'] = 'active'
    context = {
        'context': nav,
        'publications': publication,
        'cursos': courses,
    }

    return render(request, 'users/index.html', context)
